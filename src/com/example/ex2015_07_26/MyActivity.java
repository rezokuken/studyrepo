package com.example.ex2015_07_26;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyActivity extends Activity {

    private ListView listView;
    private List<String> citiesList;
    private int index = 54;
    private String someString = "new commit";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        inflateCitiesList();
        initListView();
    }

    private void initListView() {
        listView = (ListView) findViewById(R.id.list);
        ArrayAdapter<String> adapter = createCustomArrayAdapter();
        listView.setAdapter(adapter);
    }

    private ArrayAdapter<String> createCustomArrayAdapter() {
        return new ArrayAdapter<String>(this, R.layout.text_w_delete, citiesList){
            private LayoutInflater inflater =
                    (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                System.out.println("getView called " + position);
                View currentView;
                currentView =
                        convertView == null                                     ?
                                inflater.inflate(R.layout.text_w_delete, null)  :
                                convertView;

                TextView textView = (TextView) currentView.findViewById(R.id.sampleTextView);
                Button button = (Button) currentView.findViewById(R.id.sampleButton);

                textView.setText(citiesList.get(position));
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteCity(position);
                        notifyDataSetChanged();
                    }
                });
                if(position %2 == 0) {
                    currentView.setBackgroundColor(Color.BLUE);
                } else {
                    currentView.setBackgroundColor(Color.BLACK);
                }
                return currentView;
            }

            private void deleteCity(int position) {
                citiesList.remove(position);
            }
        };
    }

    private void inflateCitiesList() {
        String[] citiesArray = getResources().getStringArray((R.array.sampleArray));
        citiesList = new ArrayList<>(Arrays.asList(citiesArray));
    }
}
